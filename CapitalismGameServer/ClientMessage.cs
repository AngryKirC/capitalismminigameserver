﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
#pragma warning disable CS0649

namespace CapitalismGameServer
{
    class ClientMessage
    {
        [JsonProperty("type")]
        public string Type;

        [JsonProperty("x")]
        public int X;

        [JsonProperty("y")]
        public int Y;
    }
}
