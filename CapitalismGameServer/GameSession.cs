﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Fleck;
using Newtonsoft.Json;

namespace CapitalismGameServer
{
    class GameSession
    {
        const int TURN_TIME_LIMIT_SECONDS = 31; // Needs to be in sync with client.

        #region **** Special variables ****
        static List<GameSession> ActiveGameSessions = new List<GameSession>();

        // Simple counter which keeps track of how many game sessions we have created
        static int sessionCounter = 0;

        static Random rng = new Random();
        #endregion

        #region **** Network variables ****
        IWebSocketConnection black;
        IWebSocketConnection white;

        // Server-side ID of this game session.
        int sessionCode = 0;
        #endregion

        #region **** Gameplay variables ****
        /// <summary>
        /// This flag is set only when game ends because of someone losing/winning.
        /// </summary>
        bool IsVictory = false;
        // Player 1 is black
        // Player 2 is white
        int WhosTurn = 0;
  
        /// <summary>
        /// Money for each player.
        /// </summary>
        int[] Money = new int[2];
        /// <summary>
        /// Timer that keeps track of time player has to make a move.
        /// </summary>
        Timer turnCountdownTimer = null;
        /// <summary>
        /// Size of the game board. Board is always a square.
        /// </summary>
        int BoardSize = 5;
        /// <summary>
        /// The game board. Every square has an owner. May be neutral, black and white.
        /// </summary>
        byte[,] Board = null;
        /// <summary>
        /// Counts how many times each cell was captured.
        /// </summary>
        byte[,] BoardCaptureCounter = null;
        #endregion

        #region **** Network-related code ****
        void ConnectionDroppedHandler()
        {
            // If someone has dropped connection (e.g. closed page) notify the onther one too
            if (!IsVictory)
                EndSessionDroppedByOther();
        }

        void EndSessionDroppedByOther()
        {
            ActiveGameSessions.Remove(this);
            Console.WriteLine("Game session ended because someone dropped connection.");
            if (black.IsAvailable)
            {
                black.Send("{ \"type\": \"disconn\" }");
                black.Close();
                black = null;
            }
            if (white.IsAvailable)
            {
                white.Send("{ \"type\": \"disconn\" }");
                white.Close();
                white = null;
            }
            if (turnCountdownTimer != null)
            {
                turnCountdownTimer.Stop();
                turnCountdownTimer = null;
            }
        }

        void EndSessionVictory()
        {
            ActiveGameSessions.Remove(this);
            // Console message is being sent by PlayerWins()
            if (black.IsAvailable)
            {
                black.Send("{ \"type\": \"end\" }");
                black.Close();
                black = null;
            }
            if (white.IsAvailable)
            {
                white.Send("{ \"type\": \"end\" }");
                white.Close();
                white = null;
            }
            if (turnCountdownTimer != null)
            {
                turnCountdownTimer.Stop();
                turnCountdownTimer = null;
            }
        }

        public void NotifyMoney()
        {
            if (black.IsAvailable)
                black.Send("{ \"type\": \"money\", \"v\": " + Money[0] + " }");
            if (white.IsAvailable)
                white.Send("{ \"type\": \"money\", \"v\": " + Money[1] + " }");
        }

        public void NotifyTurn()
        {
            if (black.IsAvailable)
                black.Send("{ \"type\": \"turn\", \"id\": " + WhosTurn + " }");
            if (white.IsAvailable)
                white.Send("{ \"type\": \"turn\", \"id\": " + WhosTurn + " }");
        }

        void NotifyBoardColor(int x, int y)
        {
            if (black.IsAvailable)
                black.Send("{ \"type\": \"board\", \"x\": " + x + ", \"y\": " + y + ", \"co\": " + Board[x, y] + ", \"cc\": " + BoardCaptureCounter[x, y] + " }");
            if (white.IsAvailable)
                white.Send("{ \"type\": \"board\", \"x\": " + x + ", \"y\": " + y + ", \"co\": " + Board[x, y] + ", \"cc\": " + BoardCaptureCounter[x, y] + " }");
        }

        void SendInfoText(int plyr, string text)
        {
            string t = "{ \"type\": \"info\", \"t\": \"" + text + "\" }";
            if (plyr == 1 && black.IsAvailable)
                black.Send(t);
            if (plyr == 2 && white.IsAvailable)
                white.Send(t);
        }

        void ClientIncomingMessageHandler(byte who, string s)
        {
            try
            {
                var msg = JsonConvert.DeserializeObject<ClientMessage>(s);
                if (msg.Type == "capture") {
                    MakeAMove(msg.X, msg.Y, who);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to parse client message: " + e.Message +
                    " session " + sessionCode + " player " + who);
            }
        }

        void ClientBMessageHandler(string s)
        {
            ClientIncomingMessageHandler(2, s); // white
        }

        void ClientAMessageHandler(string s)
        {
            ClientIncomingMessageHandler(1, s); // black
        }
        #endregion

        #region **** Gameplay logic code ****
        /// <summary>
        /// Not only changes the square color/ownership server-side, but also notifies clients of it.
        /// </summary>
        void BoardChangeColor(int x, int y, int owner)
        {
            Board[x, y] = Convert.ToByte(owner);
            if (BoardCaptureCounter[x, y] < 100)
                BoardCaptureCounter[x, y]++;

            NotifyBoardColor(x, y);
        }

        /// <summary>
        /// Returns color of square specified by x, y, in case specified square doesn't exist, returns 0 instead
        /// </summary>
        byte GetSquareSafeVariant(int x, int y)
        {
            if (x < 0 || x >= BoardSize) return 0;
            if (y < 0 || y >= BoardSize) return 0;
            return Board[x, y];
        }

        /// <summary>
        /// Returns true, if a square has a neighbor with specified color nearby.
        /// </summary>
        bool HasNeighborOfColor(int x, int y, byte col)
        {
            if (GetSquareSafeVariant(x - 1, y) == col ||
                GetSquareSafeVariant(x + 1, y) == col ||
                GetSquareSafeVariant(x, y - 1) == col ||
                GetSquareSafeVariant(x, y + 1) == col)
                return true;
            return false;            
        }

        /// <summary>
        /// Returns the established price of capturing specified square.
        /// </summary>
        int GetSquarePrice(int x, int y)
        {
            int price = 4; // Price for enemy's square
            if (Board[x, y] == 0)
                price = 1; // Price for neutral square

            // Increase for every time square was captured.
            price += BoardCaptureCounter[x, y] * 2;

            return price;
        }

        /// <summary>
        /// Player 'move'/capture square logic. Switches turn if ok.
        /// </summary>
        void MakeAMove(int x, int y, byte plr)
        {
            if (IsVictory) return; // trying to cheat?
            if (WhosTurn != plr || x < 0 || y < 0 || x >= BoardSize || y >= BoardSize)
                return;
            else
            {
                byte color = GetSquareSafeVariant(x, y);
                
                if (color == plr)
                {
                    // Click on your own square.
                    //SwitchTurn();
                    return;
                }
                else
                {
                    if (HasNeighborOfColor(x, y, plr))
                    {
                        int price = GetSquarePrice(x, y);

                        if (Money[plr - 1] >= price)
                        {
                            Money[plr - 1] -= price; // Remove cash
                            BoardChangeColor(x, y, plr); // Change owner
                            SwitchTurn();
                            return;
                        }
                        else
                        {
                            // Not enough money.
                            SendInfoText(plr, "You don't have enough money.");
                        }
                    }
                    // Else, in any way this is impossible move.
                }
            }
        }

        /// <summary>
        /// Counts number of squares controlled by player.
        /// </summary>
        int CountPlayerSquares(int plyr)
        {
            byte col = Convert.ToByte(plyr);
            int result = 0;
            for (int x = 0; x < BoardSize; x++)
            {
                for (int y = 0; y < BoardSize; y++)
                {
                    if (Board[x, y] == col)
                        result++;
                }
            }
            return result;
        }

        /// <summary>
        /// Counts number of squares player is able to capture with CURRENT money.
        /// </summary>
        int GetPossibleMoves(int plyr)
        {
            byte col = Convert.ToByte(plyr);
            int money = Money[plyr - 1];
            int result = 0;

            for (int x = 0; x < BoardSize; x++)
            {
                for (int y = 0; y < BoardSize; y++)
                {
                    if (HasNeighborOfColor(x, y, col) && col != Board[x, y])
                    {
                        if (GetSquarePrice(x, y) <= money)
                            result++; // you can buy this
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// New turn logic. Counts money, win checks, etc.
        /// </summary>
        void SwitchTurn()
        {
            if (IsVictory) return; // Safety measure. Don't make moves when the game has ended

            if (WhosTurn == 0)
                WhosTurn = rng.Next(2) + 1;  // First turn of the game is chosen randomly.
            else
                WhosTurn = 2 - WhosTurn + 1;

            // Give money.
            if (WhosTurn > 0)
                Money[WhosTurn - 1] = CountPlayerSquares(WhosTurn);

            int otherOne = 2 - WhosTurn + 1;
            // Win/lose condition check
            if (GetPossibleMoves(WhosTurn) <= 0 && CountPlayerSquares(WhosTurn) < CountPlayerSquares(otherOne))
                PlayerWins(otherOne);

            if (turnCountdownTimer == null) // It's a first turn, timer is not yet created
            {
                turnCountdownTimer = new Timer(TURN_TIME_LIMIT_SECONDS * 1000);
                turnCountdownTimer.Elapsed += TurnTimeHasElapsed;
            }

            // Resets the timer interval
            turnCountdownTimer.Stop();
            turnCountdownTimer.Start();

            NotifyMoney();
            NotifyTurn();
        }

        /// <summary>
        /// Called when player doesn't make a turn in time. 
        /// </summary>
        private void TurnTimeHasElapsed(object sender, ElapsedEventArgs e)
        {
            SendInfoText(WhosTurn, "Your time is out!");
            SwitchTurn();
        }

        /// <summary>
        /// The winner is you. Or not quite you. It just happened, anyway.
        /// </summary>
        void PlayerWins(int plyr)
        {
            IsVictory = true;
            string a = "You have won!";
            string b = "You have LOST!";

            // Swap strings
            if (plyr == 2)
            {
                string c = b;
                b = a;
                a = c;
            }
            Console.WriteLine("Player " + plyr + " wins in session " + sessionCode + ".");

            // Notify all clients of this event!
            SendInfoText(1, a);
            SendInfoText(2, b);
            EndSessionVictory();
        }

        /// <summary>
        /// Initializes game logic.
        /// </summary>
        void StartGame(int boardSize)
        {
            BoardSize = boardSize;
            // Switch gamemode on client.
            if (black.IsAvailable)
                black.Send("{ \"type\": \"play\", \"color\": 1, \"size\": " + BoardSize + " }");
            if (white.IsAvailable)
                white.Send("{ \"type\": \"play\", \"color\": 2, \"size\": " + BoardSize + " }");

            // Create/initialize the board.
            Board = new byte[BoardSize, BoardSize];
            BoardCaptureCounter = new byte[BoardSize, BoardSize];
            BoardChangeColor(0, 0, 1);
            BoardChangeColor(BoardSize - 1, BoardSize - 1, 2);

            SwitchTurn();
        }
        #endregion

        /// <summary>
        /// Sets event handlers and stuff. Calls StartGame()
        /// </summary>
        public static void StartNewSession(IWebSocketConnection a, IWebSocketConnection b)
        {
            GameSession session = new GameSession();
            session.black = a;
            session.white = b;
            a.OnMessage += session.ClientAMessageHandler;
            b.OnMessage += session.ClientBMessageHandler;
            a.OnClose += session.ConnectionDroppedHandler;
            b.OnClose += session.ConnectionDroppedHandler;

            session.sessionCode = sessionCounter++;
            ActiveGameSessions.Add(session);
            Console.WriteLine("New game session started! Id: " + session.sessionCode);
            int boardSize = 4 + rng.Next(3); // from 4 to 6
            session.StartGame(boardSize);
        }
    }
}
