﻿using System;
using System.Collections.Generic;
using System.Text;
using Fleck;

namespace CapitalismGameServer
{
    class Server
    {
        //bool IsRunning = false;
        WebSocketServer wss;

        IWebSocketConnection unpairedClient = null;

        public void StartServer()
        {
            wss = new WebSocketServer("ws://0.0.0.0:5101") // 0.0.0.0 means bind to all interfaces
            {
                SupportedSubProtocols = new string[] { "game" }
            }; 
            wss.ListenerSocket.NoDelay = true;
            wss.RestartAfterListenError = true;
            wss.Start(socket =>
            {
                socket.OnOpen = () => ClientOpen(socket);
                socket.OnClose = () => ClientClose(socket);
            });
            Console.WriteLine("Server started.");
        }

        void PairClient(IWebSocketConnection socket)
        {
            if (unpairedClient == null) {
                unpairedClient = socket;
                Console.WriteLine("Unpaired client arrived.");
            }
            else
            {
                GameSession.StartNewSession(unpairedClient, socket);
                unpairedClient = null;
            }
        }

        void ClientOpen(IWebSocketConnection socket)
        {
            socket.Send("{ \"type\": \"wait\" }");
            PairClient(socket);
        }

        void ClientClose(IWebSocketConnection socket)
        {
            if (unpairedClient == socket)
            {
                unpairedClient = null; // someone got tired of waiting!
                Console.WriteLine("Unpaired client has left.");
            }
        }
    }
}
